package com.agsatu.mobilesatgas

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import androidx.fragment.app.FragmentTransaction
import com.agsatu.mobilesatgas.reminder.FragReminder
import com.agsatu.mobilesatgas.screening.FragScreening
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener  {
    lateinit var ft : FragmentTransaction
    lateinit var fragReminder : FragReminder
    lateinit var fragScreening : FragScreening
    lateinit var clBtmSheet : LinearLayout
    lateinit var btmSheetBehavior: BottomSheetBehavior<LinearLayout>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        clBtmSheet = findViewById(R.id.llBottomSheet)
        btmSheetBehavior = BottomSheetBehavior.from(clBtmSheet)
        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragReminder = FragReminder()
        fragScreening = FragScreening()
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when(p0.itemId){
            R.id.itemReminder ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragReminder).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,225))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemSkrinning ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragScreening).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemAbout -> {
                btmSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                //frameLayout.visibility = View.GONE
            }
        }

        return true
    }
}
