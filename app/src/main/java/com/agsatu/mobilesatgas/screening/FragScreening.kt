package com.agsatu.mobilesatgas.screening

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.agsatu.mobilesatgas.MainActivity
import com.agsatu.mobilesatgas.R

class FragScreening : Fragment() {
    lateinit var thisParent : MainActivity
    lateinit var v : View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_screening,container,false)

        return v

    }
}